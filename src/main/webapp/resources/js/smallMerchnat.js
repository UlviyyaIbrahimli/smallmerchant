$(document).ready(function () {

$("#advSearchBtnForSuccessSalesId").click(function (){
    advancedSearchSmSuccessTransaction();
});
$("#updateSmallMerchantInfo").click(function (){
   updateSmallMerchantInfo();
});
   });

function advancedSearchSmSuccessTransaction() {

    Swal.fire({
        title: 'Loading... ',
        html: 'Loading...  ',
        allowOutsideClick: false,
        onBeforeOpen: () => {
            Swal.showLoading()
        },
    });

    let paymentKey = $('#paymentKeyId').val();
    let transactionNo = $('#transactionNoId').val();
    let rrn = $('#rrnId').val();
    let approvalCode = $('#approvalCodeId').val();
    let description = $('#descriptionId').val();
    let resultCode = $('#resultCodeId').val();
    let cardNumber1 = $('#cardNumberId1').val();
    let cardNumber2 = $('#cardNumberId2').val();
    let amount = $('#amountId').val();
    let beginDate = $('#beginDateId').val();
    let endDate = $('#endDateId').val();
    let limit = $('#limitId').val();

    let data = {
        paymentKey: paymentKey,
        transactionNo: transactionNo,
        rrn: rrn,
        approvalCode: approvalCode,
        description: description,
        resultCode: resultCode,
        cardNumber1: cardNumber1,
        cardNumber2: cardNumber2,
        amount: amount,
        beginDate: beginDate,
        endDate: endDate,
        limit: limit
    };

    $.ajax({
        url: 'advSearchSmSuccessSales',
        type: 'GET',
        data: data,
        dataType: 'html',
        success: function (response) {
            swal.close();
            $('#smSuccessSalesTableDivId').html(response);
        }
    });


}



function updateSmallMerchantInfo() {

    let username = $("#usernameId").val();
    let description = $("#descriptionId").val();
    let autKey = $("#authenticationKeyId").val();
    let agentNo = $("#agentNumberId").val();
    let okPage = $("#okPageId").val();
    let errorPage = $("#errorPageId").val();
    let password = $("#passwordId").val();
    let webPassword = $("#webPasswordId").val();
    let data = null;
    if (password != null && webPassword != null) {
        data = {
            username: username,
            description: description,
            autKey: autKey,
            agentNo: agentNo,
            okPage: okPage,
            errorPage: errorPage,
            password: password,
            webPassword: webPassword,
        }

    } else {
        data = {
            username: username,
            description: description,
            auth_key: autKey,
            agentNo: agentNo,
            okPage: okPage,
            errorPage: errorPage,
        }
    }
    $.ajax({
        url: 'updateSmMerchantInfo',
        headers: {
            'Content-Type': 'application/json',
        },
        'data': JSON.stringify(data),
        dataType: 'json',
        type: 'POST',
        success: function (response) {
            Swal.fire({
                type: 'success',
                title: 'Success...',
                text: 'Data Update!',
            });
            },
        error: function () {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
            });
        }
    });
}