
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: fuadpashabeyli
  Date: 2021-03-11
  Time: 14:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript">
    $("#smSuccessSalesTableId").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,"destroy":true,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#lmEcommTransactionTableId_wrapper .col-md-6:eq(0)');
</script>

<table id="smSuccessSalesTableId"  class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>№</th>
        <th>PaymentKey</th>
        <th>Transaction Number</th>
        <th>Responce</th>
        <th>Approval Code</th>
        <th>Card Number</th>
        <th>Card Type</th>
        <th>Result Code</th>
        <th>RRN</th>
        <th>Transaction Type</th>
        <th>Amount</th>
        <th>Currency</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${successSales}" var="s">
        <tr>
            <td>${s.number}</td>
            <td>${s.paymentKey}</td>
            <td>${s.ecommTransactionNumber}</td>
            <td>${s.response}</td>
            <td>${s.approvalCode}</td>
            <td>${s.cardNumber}</td>
            <td>${s.cardType}</td>
            <td>${s.resultCode}</td>
            <td>${s.rrn}</td>
            <td>${s.transactionType}</td>
            <td>${s.ecommAmount}</td>
            <td>${s.currency}</td>
            <td>${s.dataDate}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>