<%--
  Created by IntelliJ IDEA.
  User: fuadpashabeyli
  Date: 2021-03-17
  Time: 13:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-lg-3 col-6">
    </div>
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
            <div class="inner">
                <h3>${todaySalesCount}</h3>
                <p>Today's Sales Count</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="${pageContext.request.contextPath}/smMerchant/todaySalesInfo" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->

    <!-- ./col -->
    <%--    <div class="col-lg-3 col-6">--%>
    <%--        <!-- small box -->--%>
    <%--        <div class="small-box bg-warning">--%>
    <%--            <div class="inner">--%>
    <%--                <h3>0</h3>--%>

    <%--                <p>0</p>--%>
    <%--            </div>--%>
    <%--            <div class="icon">--%>
    <%--                <i class="ion ion-person-add"></i>--%>
    <%--            </div>--%>
    <%--            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>--%>

    <%--&lt;%&ndash;            <a href="${pageContext.request.contextPath}/fpay/notWorkingMerchantMoreInfo" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>&ndash;%&gt;--%>
    <%--        </div>--%>
    <%--    </div>--%>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
            <div class="inner">
                <h3>${todaySalesAmount}</h3>

                <p>Today's Sales amount</p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
            <a href="${pageContext.request.contextPath}/smMerchant/todaySalesInfo" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-6">
        <%--        <!-- small box -->--%>
        <%--        <div class="small-box bg-danger">--%>
        <%--            <div class="inner">--%>
        <%--                <h3>0</h3>--%>

        <%--                <p>0</p>--%>
        <%--            </div>--%>
        <%--            <div class="icon">--%>
        <%--                <i class="ion ion-pie-graph"></i>--%>
        <%--            </div>--%>
        <%--            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>--%>
        <%--        </div>--%>
    </div>
    <!-- ./col -->
</div>
