<%--
  Created by IntelliJ IDEA.
  User: fuadpashabeyli
  Date: 2021-03-11
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->


        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-chart-pie"></i>
                <p>
                    Sales
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="${pageContext.request.contextPath}/smMerchant/smSuccessSales"
                       class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Total Success Sales</p>
                    </a>
                </li>
                <%--                <li class="nav-item">--%>
                <%--                    <a href="${pageContext.request.contextPath}/fpay/totalBillingTurnover" class="nav-link">--%>
                <%--                        <i class="far fa-circle nav-icon"></i>--%>
                <%--                        <p>Total Billing Turnover</p>--%>
                <%--                    </a>--%>
                <%--                </li>--%>

                <%--                <li class="nav-item">--%>
                <%--                    <a href="${pageContext.request.contextPath}/fpay/agentReport" class="nav-link">--%>
                <%--                        <i class="far fa-circle nav-icon"></i>--%>
                <%--                        <p>Agent Report</p>--%>
                <%--                    </a>--%>
                <%--                </li>--%>
                <%--                <li class="nav-item">--%>
                <%--                    <a href="${pageContext.request.contextPath}/fpay/merchantReport" class="nav-link">--%>
                <%--                        <i class="far fa-circle nav-icon"></i>--%>
                <%--                        <p>Merchant Report</p>--%>
                <%--                    </a>--%>
                <%--                </li>--%>

            </ul>
        </li>
<%--        <li class="nav-item">--%>
<%--            <a href="#" class="nav-link">--%>
<%--                <i class="nav-icon fas fa-tree"></i>--%>
<%--                <p>--%>
<%--                    Business Development--%>
<%--                    <i class="fas fa-angle-left right"></i>--%>
<%--                </p>--%>
<%--            </a>--%>
<%--            <ul class="nav nav-treeview">--%>
                <%--                <li class="nav-item">
                                    <a href="pages/UI/general.html" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Merchant list</p>
                                    </a>
                                </li>--%>
                <%--                <li class="nav-item">--%>
                <%--                    <a href="${pageContext.request.contextPath}/fpay/importOfflineBillingDataView" class="nav-link">--%>
                <%--                        <i class="far fa-circle nav-icon"></i>--%>
                <%--                        <p>Import Billing Data</p>--%>
                <%--                    </a>--%>
                <%--                </li>--%>
                <%--                    <li class="nav-item">--%>
                <%--                        <a href="${pageContext.request.contextPath}/fpay/offlineBillingData" class="nav-link">--%>
                <%--                            <i class="far fa-circle nav-icon"></i>--%>
                <%--                            <p>Offline Billing Data </p>--%>
                <%--                        </a>--%>
                <%--                    </li>--%>
                <%--                <li class="nav-item">--%>
                <%--                    <a href="${pageContext.request.contextPath}/fpay/newMerchant" class="nav-link">--%>
                <%--                        <i class="far fa-circle nav-icon"></i>--%>
                <%--                        <p>New Merchant </p>--%>
                <%--                    </a>--%>
                <%--                </li>--%>
                <%--                <li class="nav-item">--%>
                <%--                    <a href="${pageContext.request.contextPath}/fpay/merchantList" class="nav-link">--%>
                <%--                        <i class="far fa-circle nav-icon"></i>--%>
                <%--                        <p>Merchant List</p>--%>
                <%--                    </a>--%>
                <%--                </li>--%>
                <%--                    <li class="nav-item">--%>
                <%--                        <a href="${pageContext.request.contextPath}/fpay/webUserList" class="nav-link">--%>
                <%--                            <i class="far fa-circle nav-icon"></i>--%>
                <%--                            <p>Agent List</p>--%>
                <%--                        </a>--%>
                <%--                    </li>--%>


                <%--   <li class="nav-item">
                       <a href="pages/UI/icons.html" class="nav-link">
                           <i class="far fa-circle nav-icon"></i>
                           <p>New merchant</p>
                       </a>
                   </li>
                   <li class="nav-item">
                       <a href="pages/UI/buttons.html" class="nav-link">
                           <i class="far fa-circle nav-icon"></i>
                           <p>Certificate list</p>
                       </a>
                   </li>
                   <li class="nav-item">
                       <a href="pages/UI/sliders.html" class="nav-link">
                           <i class="far fa-circle nav-icon"></i>
                           <p>New Certificate</p>
                       </a>
                   </li>
                   <li class="nav-item">
                       <a href="pages/UI/modals.html" class="nav-link">
                           <i class="far fa-circle nav-icon"></i>
                           <p>Merchant Mid</p>
                       </a>
                   </li>
                   <li class="nav-item">
                       <a href="pages/UI/navbar.html" class="nav-link">
                           <i class="far fa-circle nav-icon"></i>
                           <p>SM Total Success Transaction</p>
                       </a>
                   </li>
                   <li class="nav-item">
                       <a href="pages/UI/timeline.html" class="nav-link">
                           <i class="far fa-circle nav-icon"></i>
                           <p>LM Total Success Transaction</p>
                       </a>
                   </li>
                   <li class="nav-item">
                       <a href="pages/UI/ribbons.html" class="nav-link">
                           <i class="far fa-circle nav-icon"></i>
                           <p>Nakhchivan Merchant's Report</p>
                       </a>
                   </li>--%>
<%--            </ul>--%>
<%--        </li>--%>

        <%--        <li class="nav-item">--%>
        <%--            <a href="#" class="nav-link">--%>
        <%--                <i class="nav-icon fas fa-tree"></i>--%>
        <%--                <p>--%>
        <%--                    Marketing--%>
        <%--                    <i class="fas fa-angle-left right"></i>--%>
        <%--                </p>--%>
        <%--            </a>--%>
        <%--            <ul class="nav nav-treeview">--%>
        <%--                <li class="nav-item">--%>
        <%--                    <a href="pages/UI/general.html" class="nav-link">--%>
        <%--                        <i class="far fa-circle nav-icon"></i>--%>
        <%--                        <p>Promo code</p>--%>
        <%--                    </a>--%>
        <%--                </li>--%>
        <%--            </ul>--%>
        <%--        </li>--%>

<%--        <li class="nav-item">--%>
<%--            <a href="#" class="nav-link">--%>
<%--                <i class="nav-icon fas fa-tree"></i>--%>
<%--                <p>--%>
<%--                    Customer Support--%>
<%--                    <i class="fas fa-angle-left right"></i>--%>
<%--                </p>--%>
<%--            </a>--%>
<%--            <ul class="nav nav-treeview">--%>
                <%--                <li class="nav-item">
                                    <a href="${pageContext.request.contextPath}/fpay/lmEcommTransaction" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Small Merchant Transaction</p>
                                    </a>
                                </li>--%>
                <%--                <li class="nav-item">--%>
                <%--                    <a href="${pageContext.request.contextPath}/fpay/lmEcommTransaction" class="nav-link">--%>
                <%--                        <i class="far fa-circle nav-icon"></i>--%>
                <%--                        <p>Large Merchant Transaction</p>--%>
                <%--                    </a>--%>
                <%--                </li>--%>
                <%--                <li class="nav-item">--%>
                <%--                    <a href="${pageContext.request.contextPath}/fpay/billingTable" class="nav-link">--%>
                <%--                        <i class="far fa-circle nav-icon"></i>--%>
                <%--                        <p>Billing Data</p>--%>
                <%--                    </a>--%>
                <%--                </li>--%>
                <%--                <li class="nav-item">--%>
                <%--                    <a href="${pageContext.request.contextPath}/fpay/billingQueue" class="nav-link">--%>
                <%--                        <i class="far fa-circle nav-icon"></i>--%>
                <%--                        <p>Billing Data </p>--%>
                <%--                    </a>--%>
                <%--                </li>--%>
                <%--                    <li class="nav-item">--%>
                <%--                        <a href="${pageContext.request.contextPath}/fpay/billingError" class="nav-link">--%>
                <%--                            <i class="far fa-circle nav-icon"></i>--%>
                <%--                            <p>Billing Error </p>--%>
                <%--                        </a>--%>
                <%--                    </li>--%>
                <%--                <li class="nav-item">--%>
                <%--                    <a href="pages/UI/general.html" class="nav-link">--%>
                <%--                        <i class="far fa-circle nav-icon"></i>--%>
                <%--                        <p>Billings Error</p>--%>
                <%--                    </a>--%>
                <%--                </li>--%>
                <%--                <li class="nav-item">
                                    <a href="pages/UI/general.html" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Test Transactions</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="pages/UI/general.html" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Contact List</p>
                                    </a>
                                </li>--%>
                <%--                <li class="nav-item">--%>
                <%--                    <a href="pages/UI/general.html" class="nav-link">--%>
                <%--                        <i class="far fa-circle nav-icon"></i>--%>
                <%--                        <p>Bin List</p>--%>
                <%--                    </a>--%>
                <%--                </li>--%>

<%--            </ul>--%>
<%--        </li>--%>

        <%--        <li class="nav-item">--%>
        <%--            <a href="#" class="nav-link">--%>
        <%--                <i class="nav-icon fas fa-tree"></i>--%>
        <%--                <p>--%>
        <%--                    Product Development--%>
        <%--                    <i class="fas fa-angle-left right"></i>--%>
        <%--                </p>--%>
        <%--            </a>--%>
        <%--            <ul class="nav nav-treeview">--%>
        <%--                <li class="nav-item">--%>
        <%--                    <a href="pages/UI/general.html" class="nav-link">--%>
        <%--                        <i class="far fa-circle nav-icon"></i>--%>
        <%--                        <p>SM Total Success Transaction</p>--%>
        <%--                    </a>--%>
        <%--                </li>--%>
        <%--                <li class="nav-item">--%>
        <%--                    <a href="pages/UI/general.html" class="nav-link">--%>
        <%--                        <i class="far fa-circle nav-icon"></i>--%>
        <%--                        <p>LM Total Success Transaction</p>--%>
        <%--                    </a>--%>
        <%--                </li>--%>
        <%--            </ul>--%>
        <%--        </li>--%>

        <li class="nav-header">Settings</li>
        <%--        <li class="nav-item">--%>
        <%--            <a href="#" class="nav-link">--%>
        <%--                <i class="nav-icon far fa-envelope"></i>--%>
        <%--                <p>--%>
        <%--                    Information Technology--%>
        <%--                    <i class="fas fa-angle-left right"></i>--%>
        <%--                </p>--%>
        <%--            </a>--%>
        <%--            <ul class="nav nav-treeview">--%>
        <%--                <li class="nav-item">--%>
        <%--                    <a href="pages/mailbox/mailbox.html" class="nav-link">--%>
        <%--                        <i class="far fa-circle nav-icon"></i>--%>
        <%--                        <p>Change Merchant Status</p>--%>
        <%--                    </a>--%>
        <%--                </li>--%>
        <%--                <li class="nav-item">--%>
        <%--                    <a href="pages/mailbox/compose.html" class="nav-link">--%>
        <%--                        <i class="far fa-circle nav-icon"></i>--%>
        <%--                        <p>Change Merchant config Status</p>--%>
        <%--                    </a>--%>
        <%--                </li>--%>
        <%--                <li class="nav-item">--%>
        <%--                    <a href="pages/mailbox/read-mail.html" class="nav-link">--%>
        <%--                        <i class="far fa-circle nav-icon"></i>--%>
        <%--                        <p>Change Card type status</p>--%>
        <%--                    </a>--%>
        <%--                </li>--%>
        <%--                <li class="nav-item">--%>
        <%--                    <a href="pages/mailbox/read-mail.html" class="nav-link">--%>
        <%--                        <i class="far fa-circle nav-icon"></i>--%>
        <%--                        <p>Change Web user status</p>--%>
        <%--                    </a>--%>
        <%--                </li>--%>
        <%--                <li class="nav-item">--%>
        <%--                    <a href="pages/mailbox/read-mail.html" class="nav-link">--%>
        <%--                        <i class="far fa-circle nav-icon"></i>--%>
        <%--                        <p>Change Mie Plugin status</p>--%>
        <%--                    </a>--%>
        <%--                </li>--%>
        <%--                <li class="nav-item">--%>
        <%--                    <a href="pages/mailbox/read-mail.html" class="nav-link">--%>
        <%--                        <i class="far fa-circle nav-icon"></i>--%>
        <%--                        <p>Grant Banks</p>--%>
        <%--                    </a>--%>
        <%--                </li>--%>
        <%--                <li class="nav-item">--%>
        <%--                    <a href="pages/mailbox/read-mail.html" class="nav-link">--%>
        <%--                        <i class="far fa-circle nav-icon"></i>--%>
        <%--                        <p>Grant Bank List</p>--%>
        <%--                    </a>--%>
        <%--                </li>--%>
        <%--                <li class="nav-item">--%>
        <%--                    <a href="pages/mailbox/read-mail.html" class="nav-link">--%>
        <%--                        <i class="far fa-circle nav-icon"></i>--%>
        <%--                        <p>IntrApp User</p>--%>
        <%--                    </a>--%>
        <%--                </li>--%>
        <%--                <li class="nav-item">--%>
        <%--                    <a href="pages/mailbox/read-mail.html" class="nav-link">--%>
        <%--                        <i class="far fa-circle nav-icon"></i>--%>
        <%--                        <p>Block Description</p>--%>
        <%--                    </a>--%>
        <%--                </li>--%>
        <%--                <li class="nav-item">--%>
        <%--                    <a href="pages/mailbox/read-mail.html" class="nav-link">--%>
        <%--                        <i class="far fa-circle nav-icon"></i>--%>
        <%--                        <p>Block Ip</p>--%>
        <%--                    </a>--%>
        <%--                </li>--%>
        <%--            </ul>--%>
        <%--        </li>--%>
        <%--        <li class="nav-item">--%>
        <%--            <a href="${pageContext.request.contextPath}/fpay/userList" class="nav-link" class="nav-link">--%>
        <%--                <i class="far fa-circle nav-icon"></i>--%>
        <%--                <p>User List</p>--%>
        <%--            </a>--%>
        <%--        </li>--%>
        <%--        <li class="nav-item">--%>
        <%--            <a href="${pageContext.request.contextPath}/fpay/newUser" class="nav-link" class="nav-link">--%>
        <%--                <i class="far fa-circle nav-icon"></i>--%>
        <%--                <p>New User</p>--%>
        <%--            </a>--%>
        <%--        </li>--%>
        <li class="nav-item">
            <a href="${pageContext.request.contextPath}/smMerchant/getSmallMerchantInfo" class="nav-link" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Change Merchant Info</p>
            </a>
        </li>


        <%--        <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-search"></i>
                        <p>
                            Search
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="pages/search/simple.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Simple Search</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="pages/search/enhanced.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Enhanced</p>
                            </a>
                        </li>
                    </ul>
                </li>--%>


    </ul>
</nav>
