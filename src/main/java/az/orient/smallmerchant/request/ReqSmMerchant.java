package az.orient.smallmerchant.request;

import lombok.Data;

@Data
public class ReqSmMerchant {
    private Long idWebUser;
    private String username;
    private String autKey;
    private String agentNo;
    private String okPage;
    private String errorPage;
    private String description;
    private String password;
    private String webPassword;

}
