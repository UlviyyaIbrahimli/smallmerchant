package az.orient.smallmerchant.dao;

import az.orient.smallmerchant.model.AdmUser;
import az.orient.smallmerchant.model.SmallMerchant;
import az.orient.smallmerchant.request.ReqSmMerchant;

public interface AdmUserDao {
    Long getSmMerchantIdByName(AdmUser admUser);
    void changeSmMerchantInfo(ReqSmMerchant reqUser) throws Exception;

    AdmUser getUserByUsername(String username);
    SmallMerchant getSmMerchantInfoByName(String name);
}
