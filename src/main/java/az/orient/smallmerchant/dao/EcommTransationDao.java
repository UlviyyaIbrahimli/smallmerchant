package az.orient.smallmerchant.dao;

import az.orient.smallmerchant.model.SearchParameter;
import az.orient.smallmerchant.model.SmMerchantEcommTransaction;

import java.util.List;


public interface EcommTransationDao {
    List<SmMerchantEcommTransaction> getTodaySalesMoreInfo(String name);

    Double getToDaySalesAmount(String name) throws Exception;

    Integer todaySalesCount(String name) throws Exception;

    List<SmMerchantEcommTransaction> getSuccessTransaction(String name) throws Exception;

    List<SmMerchantEcommTransaction> getSuccessSalesBySearchPr(SearchParameter searchParameter, int limit, String name) throws Exception;
}
