package az.orient.smallmerchant.dao.impl;

import az.orient.smallmerchant.dao.EcommTransationDao;
import az.orient.smallmerchant.model.SearchParameter;
import az.orient.smallmerchant.model.SmMerchantEcommTransaction;
import az.orient.smallmerchant.sql.SearchConstants;
import az.orient.smallmerchant.sql.SelectConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.QueryTimeoutException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EcommTransactionDaoImpl implements EcommTransationDao {
    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(EcommTransactionDaoImpl.class);

    private String addParamToSql(String paramName, String paramValue, List<Object> params) {
        if (paramValue == null || paramValue.trim().isEmpty())
            return "";

        params.add("%" + paramValue + "%");
        return " and " + paramName + " like ? ";
    }

    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

    private String addParamToSqlFullText(String paramName, String paramValue, List<Object> params) {
        if (paramValue == null || paramValue.trim().isEmpty())
            return "";

        params.add(paramValue);
        return " and match(" + paramName + ") against(?) ";
    }

    private String addParamToSqlEqual(String paramName, String paramValue, List<Object> params) {
        if (paramValue == null || paramValue.trim().isEmpty())
            return "";

        params.add(paramValue);
        return " and " + paramName + " = ? ";
    }

    private String addParamToSql(String paramName, int paramValue, List<Object> params) {
        if (paramValue < 0)
            return "";

        params.add(paramValue);
        return " and " + paramName + " = ? ";
    }

    private Date getDateFromStr(String dateStr) {
        try {
            return new Date(sdf.parse(dateStr).getTime());
        } catch (ParseException e) {
            return null;
        }
    }

    @Autowired
    DataSource dataSource;

    @Override
    public List<SmMerchantEcommTransaction> getTodaySalesMoreInfo(String name) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String query = SelectConstants.getTodaySalesMoreInfo;
        List<SmMerchantEcommTransaction> result =
                jdbcTemplate.query(query, new Object[]{name}, new BeanPropertyRowMapper<>(SmMerchantEcommTransaction.class));
        return result;
    }

    @Override
    public Double getToDaySalesAmount(String name) throws Exception {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String query = SelectConstants.getToDaySalesAmount;
        Double result = jdbcTemplate.queryForObject(query, new Object[]{name}, Double.class);
        return result;
    }

    @Override
    public Integer todaySalesCount(String name) throws Exception {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String query = SelectConstants.todaySalesCount;
        Integer result =
                jdbcTemplate.queryForObject(query, new Object[]{name}, Integer.class);
        System.out.println(result + "  result");
        return result;
    }

    @Override
    public List<SmMerchantEcommTransaction> getSuccessTransaction(String name) throws Exception {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String query = SelectConstants.getSmallMerchantSuccessTransaction;
        List<SmMerchantEcommTransaction> result =
                jdbcTemplate.query(query, new Object[]{name}, new BeanPropertyRowMapper(SmMerchantEcommTransaction.class));
        return result;
    }

    @Override
    public List<SmMerchantEcommTransaction> getSuccessSalesBySearchPr(SearchParameter searchParameter, int limit, String name) throws Exception {
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            jdbcTemplate.setQueryTimeout(300);

            String paramSql = " ";
            String paramSqlBill = " ";

            List<Object> params = new ArrayList<Object>();

            /*
             * paramSql += addParamToSql("m.merchant_display_name",
             * searchParameter.getName(), params);
             */

            if (searchParameter.getDescription() != null && !searchParameter.getDescription().isEmpty()) {
                paramSql += addParamToSql("et.description", searchParameter.getDescription(), params);

            }

            if (searchParameter.getPaymentKey().contains("\n")) {

                String sqlParams = "'" + searchParameter.getPaymentKey().replace("\n", "','") + "'";

                paramSql += " and et.payment_key IN (" + sqlParams + ") ";
                // params.add(sqlParams);
            } else {
                if (searchParameter.getPaymentKey() != null && searchParameter.getPaymentKey().length() == 36)
                    paramSql += addParamToSqlEqual("et.payment_key", searchParameter.getPaymentKey(), params);
                else
                    paramSql += addParamToSql("et.payment_key", searchParameter.getPaymentKey(), params);
            }
            if (searchParameter.getEcommTransactionNo().contains("\n")) {

                String sqlParams = "'" + searchParameter.getEcommTransactionNo().replace("\n", "','") + "'";

                paramSql += " and et.ecomm_transaction_no IN (" + sqlParams + ") ";
                // params.add(sqlParams);
            } else {
                paramSql += addParamToSqlEqual("et.ecomm_transaction_no", searchParameter.getEcommTransactionNo(),
                        params);
            }

            if (searchParameter.getRrn().contains("\n")) {

                String sqlParams = "'" + searchParameter.getRrn().replace("\n", "','") + "'";

                paramSql += " and et.rrn IN (" + sqlParams + ") ";
                // params.add(sqlParams);
            } else {
                paramSql += addParamToSqlEqual("et.rrn", searchParameter.getRrn(), params);
            }

            if (searchParameter.getApprovalCode().contains("\n")) {

                String sqlParams = "'" + searchParameter.getApprovalCode().replace("\n", "','") + "'";

                paramSql += " and et.approval_code IN (" + sqlParams + ") ";
                // params.add(sqlParams);
            } else {
                paramSql += addParamToSqlEqual("et.approval_code", searchParameter.getApprovalCode(), params);
            }

            paramSql += addParamToSql("et.result_code", searchParameter.getResultCode(), params);


            if (searchParameter.getAmount() != null) {
                int amount = (int) (searchParameter.getAmount() * 100);
                paramSql += addParamToSql("et.amount", amount, params);
            }

            if (searchParameter.getBegin() != null && !searchParameter.getBegin().trim().isEmpty()) {

                Date dt = getDateFromStr(searchParameter.getBegin());
                if (dt != null) {
                    paramSql += " and et.data_date >= ? ";
                    params.add(dt);
                }
            }

            if (searchParameter.getEnd() != null && !searchParameter.getEnd().trim().isEmpty()) {
                Date dt = getDateFromStr(searchParameter.getEnd());
                if (dt != null) {
                    paramSql += " and et.data_date <= ? ";
                    params.add(dt);
                }
            }

            String cardNumber = "";

            if (searchParameter.getCardNumber() != null && !searchParameter.getCardNumber().trim().isEmpty()) {
                cardNumber = searchParameter.getCardNumber() + "******";

            }
            if (searchParameter.getCardNumber2() != null && !searchParameter.getCardNumber2().trim().isEmpty()) {
                cardNumber += searchParameter.getCardNumber2();
            }

            if (cardNumber != null && !cardNumber.isEmpty()) {
                if (cardNumber.endsWith("******")) {
                    paramSql += " and card_number like ?";
                    params.add(cardNumber + "%");
                } else {
                    paramSql += " and card_number = ?";
                    params.add(cardNumber);
                }
            }
            paramSql += " and wu.username =?";
            params.add(name);
            String prms = "";
            for (Object obj : params) {
                prms += obj + " ";
            }
            LOGGER.info("all param values: " + prms);

            String querySql = SearchConstants.getSmSuccessSalesSearchParameter
                    .replace("[paramSql]", paramSql)
                    /* .replace("[limit2]", limit2 + "") */
                    .replace("[paramSqlBill]", paramSqlBill).replace("[limit]", limit + "");
            // m.ecomm_type IN (1,3) and

            LOGGER.info("largeMerchantSearch query: " + querySql);
            System.out.println("web user  name  " + name);

            List<SmMerchantEcommTransaction> smSuccessSales = jdbcTemplate.query(querySql, params.toArray(),
                    new BeanPropertyRowMapper(SmMerchantEcommTransaction.class)); //
            LOGGER.info("smallMerchantSearch query result: " + smSuccessSales.size());

            return smSuccessSales;
        } catch (QueryTimeoutException ex) {
            LOGGER.error("Sql timeout exception", ex);
            return null;
        } catch (Exception e) {
            throw e;
        }

    }


}
