package az.orient.smallmerchant.dao.impl;

import az.orient.smallmerchant.dao.AdmUserDao;
import az.orient.smallmerchant.model.AdmUser;
import az.orient.smallmerchant.model.SmallMerchant;
import az.orient.smallmerchant.request.ReqSmMerchant;
import az.orient.smallmerchant.security.PasswordHash;
import az.orient.smallmerchant.sql.SelectConstants;
import az.orient.smallmerchant.sql.UpdateConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

@Service
public class AdmUserDaoImpl  implements AdmUserDao {
    @Autowired
    DataSource dataSource;
    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(AdmUserDaoImpl.class);

    @Override
    public Long getSmMerchantIdByName(AdmUser admUser) {
        JdbcTemplate jdbcTemplate= new JdbcTemplate(dataSource);
        String query=SelectConstants.getIdOfSmMerchantByName;
        Long id=jdbcTemplate.queryForObject(query, new Object[]{admUser.getUsername()},Long.class);
        return id;
    }

    @Override
    public void changeSmMerchantInfo(ReqSmMerchant reqUser) throws Exception {
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            String sql = UpdateConstants.changeSmallMerchantInfo;
            String hashPassword = PasswordHash.createHash(reqUser.getPassword());
            String hashWebPassword = PasswordHash.createHash(reqUser.getWebPassword());
            System.out.println(" ddv "+reqUser.getPassword()+"  drgf  "+hashPassword);
            jdbcTemplate.update(sql, new Object[]{reqUser.getUsername(), reqUser.getDescription(),
                    reqUser.getAgentNo(), reqUser.getAutKey(), reqUser.getOkPage(), reqUser.getErrorPage(), hashPassword,
                    hashWebPassword, reqUser.getIdWebUser()});

            LOGGER.info("New password  " + sql + " \n New password HASH form: " + hashPassword);
        } catch (Exception e) {
            LOGGER.error("Change password error ", e);
        }
    }

    @Override
    public AdmUser getUserByUsername(String username) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = SelectConstants.getSmByUsername;
        List<AdmUser> admUserList =
                jdbcTemplate.query(sql, new Object[]{username}, new BeanPropertyRowMapper(AdmUser.class));
        if (!admUserList.isEmpty()) {
            return admUserList.get(0);
        }
        return null;
    }

    @Override
    public SmallMerchant getSmMerchantInfoByName(String name) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = SelectConstants.getMerchantInfoByName;
        List<SmallMerchant> admUserList =
                jdbcTemplate.query(sql, new Object[]{name}, new BeanPropertyRowMapper(SmallMerchant.class));
        if (!admUserList.isEmpty()) {
            return admUserList.get(0);
        }
        return null;
    }
}
