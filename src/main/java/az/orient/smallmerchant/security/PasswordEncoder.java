package az.orient.smallmerchant.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

public class PasswordEncoder extends BCryptPasswordEncoder {

    public PasswordEncoder() {
        super();
        // TODO Auto-generated constructor stub
    }

    public PasswordEncoder(int strength, SecureRandom random) {
        super(strength, random);
        // TODO Auto-generated constructor stub
    }

    public PasswordEncoder(int strength) {
        super(strength);
        // TODO Auto-generated constructor stub
    }

    public String getHashPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);

        // System.out.println(hashedPassword);
        return hashedPassword;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder#encode
     * (java.lang.CharSequence)
     */
    @Override
    public String encode(CharSequence rawPassword) {
        // TODO Auto-generated method stub

        try {
            return az.orient.smallmerchant.security.PasswordHash
                    .createHash(rawPassword.toString().toCharArray());
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder#matches
     * (java.lang.CharSequence, java.lang.String)
     */
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        try {
            return az.orient.smallmerchant.security.PasswordHash.validatePassword(rawPassword.toString()
                    .toCharArray(), encodedPassword);
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
}
