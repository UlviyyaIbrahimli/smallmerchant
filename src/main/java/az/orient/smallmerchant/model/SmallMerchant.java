package az.orient.smallmerchant.model;

import lombok.Data;

import java.util.Date;

@Data
public class SmallMerchant {
    private Long idWebUser;
    private String username;
    private String description;
    private String password;
    private Date webPassword;
    private Double agentNumber;
    private String authenticationKey;
    private String status;
    private String okPage;
    private String errorPage;


}