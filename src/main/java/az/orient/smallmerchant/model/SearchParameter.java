package az.orient.smallmerchant.model;

import lombok.Data;

@Data
public class SearchParameter {

	private Long idMerchant;
	private String name;
	private String paymentKey;
	private String begin;
	private String end;
	private String cardType;
	private String cardNumber;
	private String cardNumber2;
	private String resultCode;
	private String rrn;
	private String approvalCode;
	private String ecommTransactionNo;
	private Double amount;
	private String description;
	private Integer status;
	private Integer billingStatus;
	private Integer retryCount;
	private String sessionId;
	private String searchKey;
	private String amount2;
	private Long idWebUser;
	private String idWebUserArray;


}
