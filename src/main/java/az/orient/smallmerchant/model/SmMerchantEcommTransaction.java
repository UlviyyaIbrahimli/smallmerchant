package az.orient.smallmerchant.model;

import lombok.Data;

import java.util.Date;

@Data
public class SmMerchantEcommTransaction {
    private Long number;
    private Long idEcommTransaction;
    private  Long idMerchantId;
    private Long idMerchantConfig;
    private Date dataDate;
    private  String paymentKey;
    private  Double ecommAmount;
    private String clientIp;
    private String response;
    private String currency;
    private String ecommTransactionNumber;
    private String cardNumber;
    private String cardType;
    private String resultCode;
    private String rrn;
    private String transactionType;
    private String approvalCode;
}
