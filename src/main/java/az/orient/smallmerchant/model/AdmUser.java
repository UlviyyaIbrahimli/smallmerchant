package az.orient.smallmerchant.model;

import lombok.Data;

import java.util.Date;

@Data
public class AdmUser {
    private Long idMerchant;
    private  String  displayName;
    private Long idAdmUser;
    private Long number;
    private String username;
    private String password;
    private String name;
    private String surname;
    private Long admRoleId;
    private  String description;
    private String roleName;
    private Date dataDate;
    private Integer active;
}
