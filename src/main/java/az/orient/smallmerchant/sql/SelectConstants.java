package az.orient.smallmerchant.sql;

public class SelectConstants {

    public  static  final String getSmByUsername= "SELECT \n" +
            "    wu.username,\n" +
            "    wu.id_web_user,\n" +
            "    wu.password,\n" +
            "    wu.web_password,\n" +
            "    m.merchant_name,\n" +
            "    m.role_name,\n" +
            "    m.id_merchant\n" +
            "FROM\n" +
            "    fizzapaydb.web_user_merchant wum\n" +
            "        INNER JOIN\n" +
            "    fizzapaydb.web_user wu ON wum.web_user_id = wu.id_web_user\n" +
            "        INNER JOIN\n" +
            "    fizzapaydb.merchant m ON m.id_merchant = wum.merchant_id\n" +
            "WHERE\n" +
            "    wu.username = ?;";

    public  static  final String  getTodaySalesMoreInfo="SELECT \n" +
            "ROW_NUMBER () OVER (ORDER BY et.data_date ASC) AS number,\n" +
            "                        et.response,\n" +
            "                        et.amount / 100 AS ecomm_amount,\n" +
            "                        et.client_ip,\n" +
            "                        et.payment_key,\n" +
            "                        (CASE\n" +
            "                            WHEN et.currency = 840 THEN 'USD'\n" +
            "                            WHEN et.currency = 944 THEN 'AZN'\n" +
            "                            ELSE 'Unknown Status'\n" +
            "                        END) AS currency,\n" +
            "                        et.approval_code,\n" +
            "                        et.data_date,\n" +
            "                        et.ecomm_transaction_no,\n" +
            "                        et.card_number,\n" +
            "                        (SELECT \n" +
            "                                ct.card_type_name\n" +
            "                            FROM\n" +
            "                                fizzapaydb.card_type ct\n" +
            "                            WHERE\n" +
            "                                ct.id_card_type = mc.card_type_id) cardType,\n" +
            "                        et.merchant_config_id,\n" +
            "                        et.result_code,\n" +
            "                        et.rrn,\n" +
            "                        et.transaction_type,\n" +
            "                        wu.id_web_user\n" +
            "FROM\n" +
            "    fizzapaydb.merchant_config mc\n" +
            "        INNER JOIN\n" +
            "    fizzapaydb.web_user wu ON wu.id_web_user = mc.web_user_id\n" +
            "        INNER JOIN\n" +
            "    fizzapaydb.ecomm_transaction et ON et.merchant_config_id = mc.id_merchant_config\n" +
            "WHERE\n" +
            "    wu.username = ? and date(et.data_date)=curdate();";

    public static  final String getToDaySalesAmount="SELECT \n" +
            "    SUM(et.amount / 100)\n" +
            "FROM\n" +
            "    fizzapaydb.merchant_config mc\n" +
            "        INNER JOIN\n" +
            "    fizzapaydb.web_user wu ON wu.id_web_user = mc.web_user_id\n" +
            "        INNER JOIN\n" +
            "    fizzapaydb.ecomm_transaction et ON et.merchant_config_id = mc.id_merchant_config\n" +
            "        INNER JOIN\n" +
            "    fizzapaydb.merchant m ON m.id_merchant = mc.merchant_id\n" +
            "WHERE\n" +
            "    wu.username = ?\n" +
            "        AND m.merchant_type_id = 2\n" +
            "        AND DATE(et.data_date) = CURDATE()\n" +
            "GROUP BY wu.username;";


    public static  final String todaySalesCount=" SELECT \n" +
            "               count(et.id_ecomm_transaction)\n" +
            "            FROM\n" +
            "                fizzapaydb.merchant_config mc\n" +
            "                    INNER JOIN\n" +
            "                fizzapaydb.web_user wu ON wu.id_web_user = mc.web_user_id\n" +
            "                    INNER JOIN\n" +
            "                fizzapaydb.ecomm_transaction et ON et.merchant_config_id = mc.id_merchant_config\n" +
            "                inner join fizzapaydb.merchant m on m.id_merchant=mc.merchant_id\n" +
            "            WHERE\n" +
            "                wu.username = ? " +
            "        AND m.merchant_type_id = 2\n" +
            "              and date(et.data_date)=curdate() group by wu.username ;";


    public  static  final  String getSmallMerchantSuccessTransaction=
            "SELECT  ROW_NUMBER () OVER (ORDER BY et.data_date ASC) AS number,\n" +
                    "                                            et.response,\n" +
                    "                                            et.amount / 100 AS ecomm_amount,\n" +
                    "                                            et.client_ip,\n" +
                    "                                            et.payment_key,\n" +
                    "                                            wu.username,\n" +
                    "                                            m.merchant_name,\n" +
                    "                                            (CASE\n" +
                    "                                                WHEN et.currency = 840 THEN 'USD'\n" +
                    "                                                WHEN et.currency = 944 THEN 'AZN'\n" +
                    "                                                ELSE 'Unknown Status'\n" +
                    "                                            END) AS currency,\n" +
                    "                                            et.approval_code,\n" +
                    "                                            et.data_date,\n" +
                    "                                            et.ecomm_transaction_no,\n" +
                    "                                            et.card_number,\n" +
                    "                                            (SELECT \n" +
                    "                                                    ct.card_type_name\n" +
                    "                                                FROM\n" +
                    "                                                    fizzapaydb.card_type ct\n" +
                    "                                                WHERE\n" +
                    "                                                    ct.id_card_type = mc.card_type_id) cardType,\n" +
                    "                                            et.merchant_config_id,\n" +
                    "                                            et.result_code,\n" +
                    "                                            et.rrn,\n" +
                    "                                            et.transaction_type\n" +
                    "                                        FROM\n" +
                    "                                            fizzapaydb.ecomm_transaction et\n" +
                    "                                                INNER JOIN\n" +
                    "                                            fizzapaydb.merchant_config mc ON mc.id_merchant_config = et.merchant_config_id\n" +
                    "                                                INNER JOIN\n" +
                    "                                            fizzapaydb.web_user wu on wu.id_web_user=mc.web_user_id\n" +
                    "                                            inner join fizzapaydb.merchant m on m.id_merchant=mc.merchant_id\n" +
                    "                                        WHERE\n" +
                    "                                            wu.username = ? \n" +
                    "                                         and m.merchant_type_id=2\n" +
                    "                                                AND et.status = 1;";


    public static final String getMerchantInfoByName =
            "            SELECT \n" +
                    "    wu.id_web_user,\n" +
                    "    wu.username,\n" +
                    "    wu.agent_no agentNumber,\n" +
                    "    wu.auth_key authenticationKey,\n" +
                    "    wu.description,\n" +
                    "    wu.error_page,\n" +
                    "    wu.ok_page,\n" +
//                    "    wu.password,\n" +
//                    "    wu.web_password,\n" +
                    "    wu.status\n" +
                    "FROM\n" +
                    "    fizzapaydb.web_user wu\n" +
                    "        INNER JOIN\n" +
                    "    fizzapaydb.web_user_merchant wm ON wm.web_user_id = wu.id_web_user\n" +
                    "        INNER JOIN\n" +
                    "    fizzapaydb.merchant m ON m.id_merchant = wm.merchant_id\n" +
                    "WHERE\n" +
                    "    wu.username = ?\n" +
                    "        AND m.merchant_type_id = 2;";



    public  static  final String getIdOfSmMerchantByName="SELECT \n" +
            "wu.id_web_user\n" +
            "FROM\n" +
            "    fizzapaydb.web_user_merchant wum\n" +
            "        INNER JOIN\n" +
            "    fizzapaydb.web_user wu ON wum.web_user_id = wu.id_web_user\n" +
            "        INNER JOIN\n" +
            "    fizzapaydb.merchant m ON m.id_merchant = wum.merchant_id\n" +
            "WHERE\n" +
            "    wu.username = ? and m.merchant_type_id=2 ;";

}
