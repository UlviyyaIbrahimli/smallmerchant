package az.orient.smallmerchant.sql;

public class SearchConstants {

    public  static  final String getSmSuccessSalesSearchParameter="SELECT  ROW_NUMBER () OVER (ORDER BY et.data_date ASC) AS number,\n" +
            "                        et.response,\n" +
            "                        et.amount / 100 AS ecomm_amount,\n" +
            "                        et.client_ip,\n" +
            "                        et.payment_key,\n" +
            "                        (CASE\n" +
            "                            WHEN et.currency = 840 THEN 'USD'\n" +
            "                            WHEN et.currency = 944 THEN 'AZN'\n" +
            "                            ELSE 'Unknown Status'\n" +
            "                        END) AS currency,\n" +
            "                        et.approval_code,\n" +
            "                        et.data_date,\n" +
            "                        et.ecomm_transaction_no,\n" +
            "                        et.card_number,\n" +
            "                        (SELECT \n" +
            "                                ct.card_type_name\n" +
            "                            FROM\n" +
            "                                fizzapaydb.card_type ct\n" +
            "                            WHERE\n" +
            "                                ct.id_card_type = mc.card_type_id) cardType,\n" +
            "                        et.merchant_config_id,\n" +
            "                        et.result_code,\n" +
            "                        et.rrn,\n" +
            "                        et.transaction_type\n" +
            "                    FROM\n" +
            "                        fizzapaydb.ecomm_transaction et\n" +
            "                            INNER JOIN\n" +
            "                        fizzapaydb.merchant_config mc ON mc.id_merchant_config = et.merchant_config_id\n" +
            "                            INNER JOIN\n" +
            "                       fizzapaydb.web_user wu  ON wu.id_web_user = mc.web_user_id\n" +
            "                       inner join fizzapaydb.merchant m on m.id_merchant=mc.merchant_id\n" +
            "                    WHERE\n" +
            "                           et.status = 1 \n" +
            "                           and  m.merchant_type_id=2\n" +
            "                         [paramSql] \n" +
            "                             [paramSqlBill] \n" +
            "                                    LIMIT \n" +
            "                                          [limit] ";
}
