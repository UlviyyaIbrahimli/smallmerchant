package az.orient.smallmerchant.sql;

public class UpdateConstants {
    public static final String changeSmallMerchantInfo = "update \n" +
            "    fizzapaydb.web_user_merchant wum\n" +
            "        INNER JOIN\n" +
            "    fizzapaydb.web_user wu ON wum.web_user_id = wu.id_web_user\n" +
            "        INNER JOIN\n" +
            "    fizzapaydb.merchant m ON m.id_merchant = wum.merchant_id\n" +
            "set wu.username=?, wu.description=?,\n" +
            "wu.agent_no=?,wu.auth_key=?, wu.ok_page=?, wu.error_page=?,wu.password=?,\n" +
            "wu.web_password=?  where\n" +
            "    wu.id_web_user = ?";


}
