package az.orient.smallmerchant.controller;

import az.orient.smallmerchant.dao.AdmUserDao;
import az.orient.smallmerchant.dao.EcommTransationDao;
import az.orient.smallmerchant.model.AdmUser;
import az.orient.smallmerchant.model.SearchParameter;
import az.orient.smallmerchant.model.SmMerchantEcommTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("smMerchant")
public class EcommController {
    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(EcommController.class);
    @Autowired
    private AdmUserDao admUserDao;

    @Autowired
    private EcommTransationDao smEcommTransactionDao;

    private String getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String user = auth.getName();
        return user;
    }

    @RequestMapping(value = "/smSuccessSales", method = RequestMethod.GET)
    public ModelAndView getSmSuccessSales() {
        LOGGER.debug(getUser() + " connected Customer Support for Small Merchant.");
        ModelAndView model = new ModelAndView("ecommTransaction/smSuccessSalesTable");
        try {
            AdmUser smMerchant = admUserDao.getUserByUsername(getUser());
            AdmUser admUser = admUserDao.getUserByUsername(getUser());
            model.addObject("todaySalesCount",smEcommTransactionDao.todaySalesCount(admUser.getUsername()));
            model.addObject("todaySalesAmount",smEcommTransactionDao.getToDaySalesAmount(admUser.getUsername()));
            List<SmMerchantEcommTransaction> successSales= smEcommTransactionDao.getSuccessTransaction(smMerchant.getUsername());
            model.addObject("successSales",successSales);
            model.addObject("admUser",admUser);
        } catch (Exception e) {
            LOGGER.error("Data loading error  ", e);
        }
        return model;
    }


    @RequestMapping(value = "/advSearchSmSuccessSales", method = RequestMethod.GET)
    public ModelAndView advSearchSmSuccessSales(HttpServletRequest request) {
        int limit = 1;
        ModelAndView model = new ModelAndView();
        model.setViewName("ecommTransaction/smSuccessSalesTableData");
        AdmUser name = admUserDao.getUserByUsername(getUser());
        SearchParameter myModel = new SearchParameter();
        int months = 0;
        try {
            if (request.getParameter("limit").equalsIgnoreCase("")) {
                limit = 1;
            } else {
                limit = Integer.parseInt(request.getParameter("limit"));
            }

            if (request.getParameter("beginDate") != "" && !request.getParameter("beginDate").isEmpty())
                myModel.setBegin(request.getParameter("beginDate"));
            if (request.getParameter("endDate") != "" && !request.getParameter("endDate").isEmpty()) {
                myModel.setEnd(request.getParameter("endDate"));
            }

            myModel.setPaymentKey(request.getParameter("paymentKey"));
            if (request.getParameter("description") != null && !request.getParameter("description").isEmpty()) {
                myModel.setDescription(request.getParameter("description"));
            }
            myModel.setResultCode(request.getParameter("resultCode"));
            myModel.setRrn(request.getParameter("rrn"));
            myModel.setApprovalCode(request.getParameter("approvalCode"));
            myModel.setEcommTransactionNo(request.getParameter("transactionNo"));
            if (request.getParameter("amount") != "" && !request.getParameter("amount").isEmpty()) {

                myModel.setAmount(Double.parseDouble(request.getParameter("amount")));
            }
            myModel.setCardNumber(request.getParameter("cardNumber1"));
            myModel.setCardNumber2(request.getParameter("cardNumber2"));
            List<SmMerchantEcommTransaction> successSales = smEcommTransactionDao.getSuccessSalesBySearchPr
                    (myModel, limit,name.getUsername());
            LOGGER.info("smallMerchantTransactionList " + successSales);
            if (successSales == null) {
                model.addObject("error", "Sorğunun icra olunmasına qoyulan müddəti aşmısınız!");
            } else {
                model.addObject("successSales", successSales);
            }
        } catch (Exception ex) {
            LOGGER.error("small Merchant Filter problem : ", ex);
        }


        return model;

    }
}
