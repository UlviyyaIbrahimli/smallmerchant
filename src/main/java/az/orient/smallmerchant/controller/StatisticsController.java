package az.orient.smallmerchant.controller;


import az.orient.smallmerchant.dao.AdmUserDao;
import az.orient.smallmerchant.dao.EcommTransationDao;
import az.orient.smallmerchant.model.AdmUser;
import az.orient.smallmerchant.model.SmMerchantEcommTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/smMerchant")
public class StatisticsController {
    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(StatisticsController.class);

    private String getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String user = auth.getName(); // get logged in username
        return user;
    }

    @Autowired
    private AdmUserDao admUserDao;

    @Autowired
    private EcommTransationDao smEcommTransationDao;


    @RequestMapping(value = "/todaySalesInfo", method = RequestMethod.GET)
    public ModelAndView getToDaySalesInfo(@RequestParam(value = "successMsg", required = false) String successMsg) {
        ModelAndView model = new ModelAndView("statistics/todaySalesInfo");
        try {
            AdmUser admUser = admUserDao.getUserByUsername(getUser());
            model.addObject("admUser", admUser);
            model.addObject("successMsg", successMsg);
            List<SmMerchantEcommTransaction> report = smEcommTransationDao.getTodaySalesMoreInfo(admUser.getUsername());
            model.addObject("todaySalesMoreInfo", report);
            model.addObject("todaySalesCount", smEcommTransationDao.todaySalesCount(admUser.getUsername()));
            model.addObject("todaySalesAmount", smEcommTransationDao.getToDaySalesAmount(admUser.getUsername()));
            LOGGER.info("todaySalesMoreInfo list size  ", report);
        } catch (Exception e) {
            LOGGER.error("Data loading error ", e);
        }
        return model;
    }


}
