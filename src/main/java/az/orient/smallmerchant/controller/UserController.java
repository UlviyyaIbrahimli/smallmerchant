package az.orient.smallmerchant.controller;

import az.orient.smallmerchant.dao.AdmUserDao;
import az.orient.smallmerchant.dao.EcommTransationDao;
import az.orient.smallmerchant.model.AdmUser;
import az.orient.smallmerchant.model.SmallMerchant;
import az.orient.smallmerchant.request.ReqSmMerchant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
@RestController
@RequestMapping("/smMerchant")
public class UserController {
    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(UserController.class);

    @Autowired
    private AdmUserDao admUserDao;

    @Autowired
    private EcommTransationDao smEcommTransactionDao;

    private String getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String user = auth.getName();
        System.out.println(user+" user");
        return user;
    }

    @RequestMapping(value = "/getSmallMerchantInfo", method = RequestMethod.GET)
    public ModelAndView getAdminInfo() {
        ModelAndView model = new ModelAndView("mailbox/smChangeInformation");
        try {
            AdmUser smMerchant = admUserDao.getUserByUsername(getUser());
            AdmUser admUser = admUserDao.getUserByUsername(getUser());
            model.addObject("todaySalesCount",smEcommTransactionDao.todaySalesCount(admUser.getUsername()));
            model.addObject("todaySalesAmount",smEcommTransactionDao.getToDaySalesAmount(admUser.getUsername()));
            SmallMerchant merchant = admUserDao.getSmMerchantInfoByName(getUser());
            model.addObject("smMerchant", merchant);
            model.addObject("admUser",admUser);
        } catch (Exception e) {
            LOGGER.error("Data loading error ",e);
        }
        return model;
    }

    @RequestMapping(value = "/updateSmMerchantInfo", method = RequestMethod.POST, consumes = "application/json", headers = "content-type=application/x-www-form-urlencoded")
    public ResponseEntity<ReqSmMerchant> changePassword(@RequestBody ReqSmMerchant reqUser) {
        try {
            AdmUser admUser = admUserDao.getUserByUsername(getUser());
            Long idSmMerchant=admUserDao.getSmMerchantIdByName(admUser);
            reqUser.setIdWebUser(idSmMerchant);
            admUserDao.changeSmMerchantInfo(reqUser);
        } catch (Exception e) {
            LOGGER.error("Change password error ",e);
        }
        return new ResponseEntity<>(reqUser, HttpStatus.ACCEPTED);
    }



}
