package az.orient.smallmerchant.controller;

import az.orient.smallmerchant.dao.AdmUserDao;
import az.orient.smallmerchant.dao.EcommTransationDao;
import az.orient.smallmerchant.model.AdmUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/smMerchant")
public class MainController {
    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(MainController.class);

    @Autowired
    AdmUserDao admUserDao;
    @Autowired
    EcommTransationDao smEcommTransactionDao;

    private String getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String user = auth.getName(); // get logged in username
        return user;
    }

    @GetMapping("/login")
    public ModelAndView login() {
        ModelAndView model = new ModelAndView("login");
        return model;

    }

    @GetMapping("/403")
    public ModelAndView accessDenied() {
        ModelAndView model = new ModelAndView("403");
        return model;
    }


    @GetMapping(value = {"/", "/admin"})
    public ModelAndView main() {
        ModelAndView model = new ModelAndView("index");
        LOGGER.info(getUser(), " admin sehifesine qoshuldu!");
        try {
            AdmUser admUser = admUserDao.getUserByUsername(getUser());
            model.addObject("todaySalesCount", smEcommTransactionDao.todaySalesCount(admUser.getUsername()));
            model.addObject("todaySalesAmount", smEcommTransactionDao.getToDaySalesAmount(admUser.getUsername()));
            model.addObject("admUser", admUser);
        } catch (Exception e) {
            LOGGER.error("Static data loading error  " + e);
        }
        return model;
    }
}
